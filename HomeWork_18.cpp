﻿#include <iostream>
#include <string>

using namespace std;

template <class T>
class Stack {

public:

    void printStack() {
        if (oldArray != nullptr) {
            cout << endl;
            cout << "The stack contains numbers:" << endl;
            cout << "===========================" << endl;

            for (int i = 0; i < size; i++) {
                if (!i) {
                    cout << oldArray[i] << " <- Top element" << endl;
                }
                else {
                    cout << oldArray[i] << endl;
                }
            }
            cout << "===========================" << endl << endl;
        }
        else {
            cout << "Stack is empty" << endl << endl;
        }
    }
    void push() {

        cout << "Insert the number:" << endl;
        cin >> element;

        size += 1;

        newArray = new T[size];

        newArray[0] = element;

        if (size > 1) {
            for (int i = 1; i < size; i++)
            {
                newArray[i] = oldArray[i - 1];
            }
            delete[] oldArray;
        }

        oldArray = new T[size];

        for (int i = 0; i < size; i++) {
            oldArray[i] = newArray[i];
        }

        printStack();
        delete[] newArray;

    }
    void pop() {
        if (oldArray != nullptr) {
            cout << "Top element: " << oldArray[0] << endl << endl;
        }
        else {
            cout << "Stack is empty" << endl << endl;

        }
    }
    void remove() {
        if (oldArray != nullptr) {
            if (size > 1) {
                newArray = new T[size - 1];

                for (int i = 0; i < size - 1; i++) {
                    newArray[i] = oldArray[i + 1];
                }

                delete[] oldArray;
                size -= 1;
                oldArray = new T[size];

                for (int i = 0; i < size; i++) {
                    oldArray[i] = newArray[i];
                }
                delete[] newArray;
                printStack();
            }
            else {
                delete[] oldArray;
                oldArray = nullptr;
                size -= 1;
                cout << "Stack is empty" << endl << endl;
            }
        }
        else {
            cout << "Stack is empty" << endl << endl;
        }

    }
private:

    int size = 0;
    T element = 0;
    T* oldArray = nullptr;
    T* newArray = nullptr;
};

int main()
{
    int menu = 0;

    Stack <int> steck;

    while (1) {

        cout << "Select an action:" << endl;
        cout << "0 - Exit" << endl;
        cout << "1 - Delete from stack" << endl;
        cout << "2 - Add to stack" << endl;
        cout << "3 - Get top element" << endl;
        cout << "4 - Print Stack" << endl << endl;

        cin >> menu;

        switch (menu) {

        case 0:
            cout << "Exit" << endl << endl;
            return 0;
            break;
        case 1:
            system("cls");
            cout << "Delete" << endl << endl;
            steck.remove();
            break;
        case 2:
            system("cls");
            cout << "Add" << endl << endl;
            steck.push();
            break;
        case 3:
            system("cls");
            cout << "Get" << endl << endl;
            steck.pop();
            break;
        case 4:
            system("cls");
            cout << "Print" << endl << endl;
            steck.printStack();
            break;
        default:
            system("cls");
            cout << "Try it again" << endl << endl;
            break;
        }
    }

    return 0;
}
